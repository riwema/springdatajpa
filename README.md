# Spring Data JPA
## Ronald Iwema - ilionx

Dit project is opgezet ten behoeve van een presentatie over Spring Data. Tijdens de presentatie zijn er een aantal proefqueries gemaakt in een voorbeeld applicatie. Dit is de voorbeeld applicatie.

### Vereisten

1. Java
2. Maven
3. MySQL

### Applicatie starten

Bouw de applicatie

    mvn install

Maak verbinding met je database

    mysql -uroot -p

Creeer een database en gebruiker met bijbehorende rechten

    create database sinterklaas;
    grant usage on *.* to sinterklaas@localhost identified by 'sinterklaaspass';
    grant all privileges on sinterklaas.* to sinterklaas@localhost;
    exit;

Laat FlyWay de tabellen en testdata opzetten

    mvn flyway:init
    mvn flyway:migrate

Draai de applicatie

    mvn jetty:run

### Informatie over gebruikte frameworks


- [Spring Data][1]
- [Spring Data JPA][2]
- [FlyWay][3]
- [Jersey][4]
- [Maven][5]
- [Jetty][6]

[1]: http://projects.spring.io/spring-data/
[2]: http://projects.spring.io/spring-data-jpa/
[3]: http://flywaydb.org/
[4]: https://jersey.java.net/
[5]: http://maven.apache.org/
[6]: http://www.eclipse.org/jetty/

### Disclaimer

Dit project is eenvoudig opgezet om snel de kracht van Spring Data te laten zien, maar het zit vol slechte ideeen ;). De hoofdlijnen:

1. Spreek geen repositories/dao's rechtstreeks aan in de laag die requests afhandelt
2. Gebruik transacties
3. Zet je testdata niet in src/main/resources
4. Gebruik unit testen






