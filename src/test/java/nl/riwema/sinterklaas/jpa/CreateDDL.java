package nl.riwema.sinterklaas.jpa;

import nl.riwema.sinterklaas.business.entity.Person;
import nl.riwema.sinterklaas.business.entity.Present;
import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaExport;

import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: riwema
 * Date: 09/07/2013
 * Time: 10:21
 * To change this template use File | Settings | File Templates.
 */
public class CreateDDL {


    public static void main (String... args) {
        Configuration config = new Configuration();

        Properties properties = new Properties();

        properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
        properties.put("hibernate.connection.url", "jdbc:mysql://localhost:3306/sinterklaas");
        properties.put("hibernate.connection.username", "sinterklaas");
        properties.put("hibernate.connection.password", "sinterklaaspass");
        properties.put("hibernate.connection.driver_class", "com.mysql.jdbc.Driver");
        properties.put("hibernate.show_sql", "true");
        config.setProperties(properties);

        config.addAnnotatedClass(Present.class);
        config.addAnnotatedClass(Person.class);


        SchemaExport schemaExport = new SchemaExport(config);
        schemaExport.setOutputFile("/Users/riwema/schema_sinterklaas.sql");
        schemaExport.setDelimiter(";");
        schemaExport.setFormat(true);

/**Just dump the schema SQLs to the console , but not execute them ***/
        schemaExport.create(true, false);
    }
}
