package nl.riwema.sinterklaas.service;

import nl.riwema.sinterklaas.business.entity.Person;

import nl.riwema.sinterklaas.business.entity.Present;
import nl.riwema.sinterklaas.integration.jpa.PersonRepository;
import nl.riwema.sinterklaas.integration.jpa.PresentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;


@Path("/sinterklaas")
@Component
public class SinterklaasAPI {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private PresentRepository presentRepository;

    @GET
    @Path("/person/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Person findPerson(@PathParam("id") String id) {
        return personRepository.findOne(Long.parseLong(id));
    }

    @GET
    @Path("/present/contains/{description}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Present> findPresentsByDescription(@PathParam("description") String description) {
        return presentRepository.findByDescriptionContains(description);
    }

    @GET
    @Path("/present/contains/{description}/person1/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Present> findPresentsByDescriptionPersonID(@PathParam("description") String description, @PathParam("id") String id) {
        return presentRepository.findByPersonIdAndDescriptionContains(Long.parseLong(id), description);
    }

    @GET
    @Path("/present/contains/{description}/person2/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Present> findPresentsByDescriptionPersonID2(@PathParam("description") String description, @PathParam("id") String id) {
        Person person = personRepository.findOne(Long.parseLong(id));
        return presentRepository.findByPersonAndDescriptionContains(person, description);
    }

    @GET
    @Path("/present/contains/{description}/person3/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Present> findPresentsByDescriptionPersonID3(@PathParam("description") String description, @PathParam("name") String name) {
        return presentRepository.findByPersonNameAndDescriptionContains(name, description);
    }

    @GET
    @Path("/person/agebetween/{age1}/{age2}/{page}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Person> findPersonn(@PathParam("age1") int age1, @PathParam("age2") int age2, @PathParam("page") int page) {
        return personRepository.findByAgeBetween(age1, age2, new PageRequest(page, 1, new Sort(Sort.Direction.DESC, "name")));
    }




}
