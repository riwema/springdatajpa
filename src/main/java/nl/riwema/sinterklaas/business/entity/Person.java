package nl.riwema.sinterklaas.business.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table( name = "PERSON" )
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "AGE")
    private Integer age;

    @OneToMany(fetch=FetchType.EAGER, mappedBy = "person")
    private List<Present> presents;

    public List<Present> getPresents() {
        return presents;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
