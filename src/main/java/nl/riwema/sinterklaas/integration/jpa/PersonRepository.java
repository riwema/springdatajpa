package nl.riwema.sinterklaas.integration.jpa;

import nl.riwema.sinterklaas.business.entity.Person;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: riwema
 * Date: 09/10/2013
 * Time: 07:04
 * To change this template use File | Settings | File Templates.
 */
public interface PersonRepository extends CrudRepository<Person, Long> {

    //@Query(value = "select p from Person p where p.id = :id")
    //List<Person> findPersonsAlternatief(String id);

    List<Person> findByAgeBetween(int age1, int age2, Pageable pageable);
}
