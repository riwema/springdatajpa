package nl.riwema.sinterklaas.integration.jpa;

import nl.riwema.sinterklaas.business.entity.Person;
import nl.riwema.sinterklaas.business.entity.Present;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: riwema
 * Date: 09/10/2013
 * Time: 07:12
 * To change this template use File | Settings | File Templates.
 */
public interface PresentRepository extends CrudRepository<Present, Long> {

       List<Present> findByDescriptionContains(String description);

       List<Present> findByPersonAndDescriptionContains(Person p, String description);

       List<Present> findByPersonIdAndDescriptionContains(Long id, String description);

       List<Present> findByPersonNameAndDescriptionContains(String name, String description);


}
