INSERT INTO PERSON (id, name, age) VALUES (1, 'Jan', 32);
INSERT INTO PERSON (id, name, age) VALUES (2, 'Marlies', 26);
INSERT INTO PERSON (id, name, age) VALUES (3, 'Johanna', 29);
INSERT INTO PERSON (id, name, age) VALUES (4, 'Kees', 31);
INSERT INTO PERSON (id, name, age) VALUES (5, 'Joost', 58);
INSERT INTO PERSON (id, name, age) VALUES (6, 'Miranda', 61);

INSERT INTO PRESENT (person_id, title, description) VALUES (1, 'Sokken', 'Zwarte sokken');
INSERT INTO PRESENT (person_id, title, description) VALUES (1, 'Sokken', 'Bruine sokken');
INSERT INTO PRESENT (person_id, title, description) VALUES (1, 'Sokken', 'Gele sokken');
INSERT INTO PRESENT (person_id, title, description) VALUES (1, 'Sokken', 'Paarse sokken');
INSERT INTO PRESENT (person_id, title, description) VALUES (1, 'Sokken', 'Groene sokken');
INSERT INTO PRESENT (person_id, title, description) VALUES (1, 'Sokken', 'Rode sokken');
INSERT INTO PRESENT (person_id, title, description) VALUES (1, 'Vogelvoer', 'Leuk pakket voor de vogels om de winter weer door te komen');

INSERT INTO PRESENT (person_id, title, description) VALUES (2, 'Paarse Kaarsen', 'Mooie grote paarse kaarsen, zonder plateau-tje, die hebben we wel :)');
INSERT INTO PRESENT (person_id, title, description) VALUES (2, 'Paardhobbelsokken', 'Ha, lekkere walleme paardhobbelsokken dus, maat 39-42 :D In leuke frisse of gezellige kleurtjes');
INSERT INTO PRESENT (person_id, title, description) VALUES (2, 'Badkussentje', 'Van die nekkussentjes, die je in bad kunt gebruiken :D');
INSERT INTO PRESENT (person_id, title, description) VALUES (2, 'Anti-voorruit-bevries-ding', 'Geen idee hoe die dingen heten, maar zo''n ding wat je voor het raam van de auto kunt doen, zodat je ''s ochtends niet hoeft te krabben.');
INSERT INTO PRESENT (person_id, title, description) VALUES (2, 'Paarse kussentjes', 'Paarse kussentjes voor op de bank');
INSERT INTO PRESENT (person_id, title, description) VALUES (2, 'Bad plateaux', 'Een plateau wat je kunt gebruiken in bad, waar je dan allemaal dingen op kunt leggen :)');

INSERT INTO PRESENT (person_id, title, description) VALUES (3, 'Sokken', 'Graag dunne sokken, maat 35-38. Vrolijke of donkere kleurtjes.');
INSERT INTO PRESENT (person_id, title, description) VALUES (3, 'Boek', 'Leuk, vrolijk, happy end-boek. Of een ander boeiend, misschien waargebeurd-verhaal, boek (oftewel tranentrekkers hahah).');
INSERT INTO PRESENT (person_id, title, description) VALUES (3, 'Iets leuks voor in huis', 'Tja wat?! Voor in de vensterbank of op tafel. Iets met kaarsen of niet hahah.. En graag iets wat niet stuk kan, vanwege onze grote boef.. Nou hoewel dat is ook niet echt nodig eigenlijk, we hebben ook een kast en dressoir waar iets op kan staan.. :)');
INSERT INTO PRESENT (person_id, title, description) VALUES (3, 'Bodycreme van Lief! voor mama', 'Het merk is "Lief! mama". Bodycreme met klaver is de beste :D');

INSERT INTO PRESENT (person_id, title, description) VALUES (4, 'Scheurkalender', 'Stamgasten of Dirk-Jan');
INSERT INTO PRESENT (person_id, title, description) VALUES (4, 'Lego', 'Voor de kids');
INSERT INTO PRESENT (person_id, title, description) VALUES (4, 'Duplo', 'Voor de kids');
INSERT INTO PRESENT (person_id, title, description) VALUES (4, 'Vogelspulletjes', 'Voor in de tuin');

INSERT INTO PRESENT (person_id, title, description) VALUES (5, 'Lekkere warme sokken', 'Niet te groot, niet te klein maar precies goed');
INSERT INTO PRESENT (person_id, title, description) VALUES (5, 'Spaarlamp', '1 of meer (c/a 5 watt )');
INSERT INTO PRESENT (person_id, title, description) VALUES (5, 'stanleymesjes', 'Alleen de mesjes');
INSERT INTO PRESENT (person_id, title, description) VALUES (5, 'Vogelvoer', 'Voor de vogels buiten');
INSERT INTO PRESENT (person_id, title, description) VALUES (5, 'Acrylaatkit wit', 'In een koker');
INSERT INTO PRESENT (person_id, title, description) VALUES (5, 'Schilderstape 22mm', 'Geschikt voor vanalles');

INSERT INTO PRESENT (person_id, title, description) VALUES (6, 'Iets leuks voor in de vensterbank', 'Het moet niet heel klein zijn. Ik Moet mij er achter kunnen verstoppen. Haha.');
INSERT INTO PRESENT (person_id, title, description) VALUES (6, 'Botten', 'Grote stevige botten om mijn tanden op te slijpen');
INSERT INTO PRESENT (person_id, title, description) VALUES (6, 'Douchemat', 'Aqua of andere kleur. Geen paars, rood, oranje of geel');
INSERT INTO PRESENT (person_id, title, description) VALUES (6, 'Kaarsen', 'In verschillende hoogtes');
INSERT INTO PRESENT (person_id, title, description) VALUES (6, 'Scheurkalender', 'Loesje of Positief denken. Geen mopjes of puzzels. Anders blijft men te lang op de wc zitten');
INSERT INTO PRESENT (person_id, title, description) VALUES (6, 'Badhanddoek', 'Aqua of kiwi');
INSERT INTO PRESENT (person_id, title, description) VALUES (6, 'Washandje', 'Aqua of kiwi');
INSERT INTO PRESENT (person_id, title, description) VALUES (6, 'Pannenlappen', 'Er heeft er een vlam gevat.');

