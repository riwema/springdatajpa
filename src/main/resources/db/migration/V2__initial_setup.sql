create table PERSON (
  id bigint not null auto_increment,
  AGE integer,
  NAME varchar(255),
  primary key (id)
);

create table PRESENT (
  ID bigint not null auto_increment,
  DESCRIPTION varchar(500),
  TITLE varchar(255),
  PERSON_ID bigint,
  primary key (ID)
);

alter table PRESENT
add index FK17D3049B9DED35A1 (PERSON_ID),
add constraint FK17D3049B9DED35A1
foreign key (PERSON_ID)
references PERSON (id);